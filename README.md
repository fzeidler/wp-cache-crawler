# wp-cache-crawler shellscript

This script warms up caches by visiting all urls in wordpress sitemaps.

It takes base urls (without scheme) as arguments, downloads page-sitemap.xml and post-sitemap.xml for each one and then outputs all URLs with their corresponding response code and the time the request took.

Call like this:

```bash
./wp-cache-crawler.sh example.com
```

compatible with WordPress 5.5
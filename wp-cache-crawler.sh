#!/bin/bash

AMP=false

while getopts ":a" opt; do
  case ${opt} in
    a ) AMP=true
      ;;
    \? ) echo "Usage: ./wp-cache-crawler.sh [-a] [DOMAINS]\n -a: also get AMP versions of all pages. default is false."
      ;;
  esac
done

for arg in "$@"
do
  DOMAIN=$arg


  wget --quiet "https://$DOMAIN/wp-sitemap.xml" --no-cache --output-document - | grep -e "<loc>" | sed 's:<loc>:\n<loc>:g' | sed -n "s/.*<loc>\(https:\/\/[^\"]\+\)<\/loc>.*/\1/p" | while read -r sitemap; do

    if [ $AMP = true ]
    then
      wget --quiet "$sitemap" --no-cache --output-document - | grep -e "<loc>" | sed 's:<loc>:\n<loc>:g' | sed -n "s/.*<loc>\(https:\/\/[^\"]\+\)<\/loc>.*/\1/p" | while read -r line; do
        printf "${line}\t\t"
        /usr/bin/time --format "\t%E" curl -A 'Cache Crawler' --location "$line" --write-out %{http_code} --silent --output /dev/null
        printf "${line}/amp\t\t"
        /usr/bin/time --format "\t%E" curl -A 'Cache Crawler' --location "${line}/amp" --write-out %{http_code} --silent --output /dev/null
      done

    else
      wget --quiet "$sitemap" --no-cache --output-document - | grep -e "<loc>" | sed 's:<loc>:\n<loc>:g' | sed -n "s/.*<loc>\(https:\/\/[^\"]\+\)<\/loc>.*/\1/p" | while read -r line; do
        printf "${line}\t\t"
        /usr/bin/time --format "\t%E" curl -A 'Cache Crawler' --location "$line" --write-out %{http_code} --silent --output /dev/null
      done
       
    fi
  done
done
 
